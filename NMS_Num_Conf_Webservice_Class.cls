/****************************************************************************************************************
*Class Name   : NMS_Num_Conf_Webservice_Class 
*Description  : POS Retirement drop 2 - NMS Number Conflict
*Date         : 27-07-2018
*Created By   : Mukul Patil
*
*       Author                 Date             Version                            Remarks
*------------------        -------------     -------------      ------------------------------------------------
* Padmasree Murali          19-01-2019           1.1                SDM Manager update
* Harivijay Boobalan        22-09-2019           1.2                CS - SFDC Alphanumeric Id Depedency Removal
* Aditi Srivastava          15-09-2019           1.3                KAM Update to BPM
* Nehal Agrawal             15-09-2019           1.4                Feasibility Rule Change
* Gokul Balusamy			06-04-2020			 1.5				Rejection of Repricing
*****************************************************************************************************************/

global class NMS_Num_Conf_Webservice_Class
{
    webservice static NMS_Opportunity update_NMS_Opportunity(NMS_Opportunity vNMS_Opportunity)
    {
        String ErrorMsg=null;
        try
        {  
           if(System.Test.isRunningTest() && vNMS_Opportunity!=null && vNMS_Opportunity.Source!=null && vNMS_Opportunity.Source.equals('FromTestClass'))
            {   
                String a=null;
                system.debug(a.equals(''));
            }
             System.debug('Inputs : '+vNMS_Opportunity);//v1.3   
            ErrorMsg = vNMS_Opportunity!=null?((vNMS_Opportunity.Source!=null && !vNMS_Opportunity.Source.equals(''))?((vNMS_Opportunity.Operation!=null && !vNMS_Opportunity.Operation.equals(''))?Label.updateSuccess:'Kindly specify the Operation'):'Source field cannot be null'):'Request Object is Null'; //V1.3 Changed the Error Message to Custom Label
            if(ErrorMsg!=null && !ErrorMsg.equals('') && ErrorMsg.equals(Label.updateSuccess))//V1.3 Changed the Error Message from Success
            {
                if(vNMS_Opportunity.Source.equals('NMS') && vNMS_Opportunity.Operation.equals('NMS_Number_Conflict_Update'))
                {
                    List<Opportunity> OptyList= new List<Opportunity>();
                    
                    OptyList=[select id,Opportunity_Id__c from Opportunity where Opportunity_Id__c=:vNMS_Opportunity.Opportunity_Id limit 1];
                    if(OptyList!=null && OptyList.size()>0)
                    {
                        Opportunity opty = new Opportunity(id=OptyList[0].id);
                        opty.IsOrderCreatedInE2E__c=vNMS_Opportunity.Extra_Input1;
                        opty.E2E_Comments__c=vNMS_Opportunity.Extra_Input1;
                        update opty;
                        vNMS_Opportunity.Error_Flag='Success';// V1.3 Changed the Flag from T to Success
                        vNMS_Opportunity.Error_Message=Label.updateSuccess;// V1.3 Changed the Message
                    }
                    else
                        //ErrorMsg='Opportunity does not exists with specified Opportunity_Id'; V1.3    
                        vNMS_Opportunity.Error_Message='Opportunity does not exists with specified Opportunity_Id';//V1.3
                    
                }   
             //version 1.1 starts
                 else if(vNMS_Opportunity.Source.equals('BPM') && vNMS_Opportunity.Operation.equals('SDM CHANGE')){
                    neotel_product__c inpProductObj = new neotel_product__c();
                    List<SDM_Details__c> sdmobjList = new List<SDM_Details__c>();
                    inpProductObj = [select id,e2e_status__c,opportunity__r.name,opportunity__r.opportunity_id__c,Service_SDM__c  from neotel_product__c where service_id__c = :vNMS_Opportunity.Extra_Input2  and opportunity__r.Opportunity_Id__c= :vNMS_Opportunity.Opportunity_Id limit 1];
                    
                    if(inpProductObj!=null )
                    {
                        sdmobjList = [select id,SDM_ID__c from SDM_Details__c where Name = : vNMS_Opportunity.Extra_Input1 limit 1];
                        if(sdmobjList!=null && !sdmobjList.isEmpty())
                        {
                        inpProductObj.Service_SDM__c=sdmobjList[0].id;
                        update inpProductObj;
                        vNMS_Opportunity.Error_Flag='Success';// V1.3 Changed the Flag from T to Success
                        vNMS_Opportunity.Error_Message=Label.updateSuccess;// V1.3 Changed the Message
                        }
                        else
                            //ErrorMsg='SDM details does not exists.'; -- V1.3
                            vNMS_Opportunity.Error_Message='SDM details does not exists.'; //V1.3
                    }
                    else
                        //ErrorMsg='Product does not exists.'; --V1.3
                        vNMS_Opportunity.Error_Message='Product does not exists.'; //V1.3         
            }   
                //version 1.1 ends
                //Version 1.2 Starts
                else if(vNMS_Opportunity.Source.equals('Geneva') && vNMS_Opportunity.Operation.equals('EARLY_TERMINATION_ASYNC')){
                    vNMS_Opportunity.Error_Message=earlyTermination(vNMS_Opportunity.Extra_Input1, vNMS_Opportunity.Extra_Input11, vNMS_Opportunity.Extra_Input12, vNMS_Opportunity.Extra_Input13);                   
                }
                else if(vNMS_Opportunity.Source.equals('Velocity') && vNMS_Opportunity.Operation.equals('MediaChangeUpdate')){
                    vNMS_Opportunity.Error_Message=mediaUpdate(vNMS_Opportunity.Opportunity_Id,vNMS_Opportunity.Extra_Input3,vNMS_Opportunity.Extra_Input4,vNMS_Opportunity.Extra_Input5,vNMS_Opportunity.Extra_Input6,vNMS_Opportunity.Extra_Input7,vNMS_Opportunity.Extra_Input8,vNMS_Opportunity.Extra_Input9,vNMS_Opportunity.Extra_Input11,vNMS_Opportunity.Extra_Input12);
                }
                else if(vNMS_Opportunity.Source.equals('BPM') && vNMS_Opportunity.Operation.equals('PROVISIONING_UPDATE')){
                    vNMS_Opportunity.Error_Message=provisioningUpdate(vNMS_Opportunity.Opportunity_Id, vNMS_Opportunity.Extra_Input2, vNMS_Opportunity.Extra_Input1, vNMS_Opportunity.Extra_Input3/*V1.5*/,vNMS_Opportunity.Extra_Input4/*V1.5*/);//Added Extra_Input4 to receive the Repricing Rejection Comments;
                }
                else if(vNMS_Opportunity.Source.equals('Velocity') && vNMS_Opportunity.Operation.equals('Service_Record_Update')){
                    vNMS_Opportunity.Error_Message=serviceRecordUpdate(vNMS_Opportunity.Opportunity_Id, vNMS_Opportunity.Extra_Input1, vNMS_Opportunity.Extra_Input2,vNMS_Opportunity.Extra_Input3);
                }
                else if(vNMS_Opportunity.Source.equals('GENEVA') && vNMS_Opportunity.Operation.equals('EMAIL_DISTRIBUTION')){
                    vNMS_Opportunity.Error_Message=emailDistribution(vNMS_Opportunity.Extra_Input1, vNMS_Opportunity.Extra_Input2, vNMS_Opportunity.Extra_Input11);         
                }
                else if(vNMS_Opportunity.Source.equals('GENEVA') && vNMS_Opportunity.Operation.equals('BILL_FIELD_SERVICE_ASYNC')){
                    vNMS_Opportunity.Error_Message=billIMACDAuto(vNMS_Opportunity.Extra_Input4, vNMS_Opportunity.Extra_Input1, vNMS_Opportunity.Extra_Input2);          
                }
                //Version 1.2 Ends
                
             //v 1.3 starts             
                else if(vNMS_Opportunity.Source.equals('BPM') && vNMS_Opportunity.Operation.equals('KAM_DETAILS_UPDATE')){
                    vNMS_Opportunity.Error_Message=kamUpdate(vNMS_Opportunity.Extra_Input1, vNMS_Opportunity.Extra_Input2, vNMS_Opportunity.Extra_Input3,vNMS_Opportunity.Extra_Input4,vNMS_Opportunity.Extra_Input5);         
                }
                
                //v 1.4 starts
                else if(vNMS_Opportunity.Source.equals('BPM') && vNMS_Opportunity.Operation.equals('Confidence_Level_Backupdate')){
                    vNMS_Opportunity.Error_Message=confidenceUpdate(vNMS_Opportunity.Extra_Input1, vNMS_Opportunity.Extra_Input2);
                }
                //v 1.4 ends
                
                ErrorMsg = vNMS_Opportunity.Error_Message;
                vNMS_Opportunity.Error_Flag='Success';
                //v 1.3 ends
                         
            }

            
            if(ErrorMsg!=null && !ErrorMsg.equals('') && !ErrorMsg.equals(Label.updateSuccess))//V1.3 Updated the Error Message from Success
            {
                System.debug('Failed with this reason : '+ErrorMsg);
                vNMS_Opportunity.Error_Flag='Failure';//V1.3 Changed the Flag from F to Failure
                vNMS_Opportunity.Error_Message=ErrorMsg;
            }
            
            }
        catch(Exception e)
        {
            System.debug('Failed at Line : '+e.getLineNumber());
            System.debug('Failed with this reason : ' + e.getMessage());
            vNMS_Opportunity.Error_Flag='Failure'; // V1.3 Changed the Flag from F to Failure
            vNMS_Opportunity.Error_Message=e.getMessage();
        }
        return vNMS_Opportunity;
    }
    
/*V1.2 Starts*/

/*-------------------------------------------------------------------------------------------------------------------------*
 * Method Name : earlyTermination
 * Description : This Method will receive the Penalty Charges Response from Geneva and will update the same in the Penalty
 *          record (NFT Intermediate Product)
 * Attributes  : vNMS_Opportunity.Extra_Input1  = pnltID (Penalty Id)
 *          vNMS_Opportunity.Extra_Input11 = status (Completion Status)
 *          vNMS_Opportunity.Extra_Input12 = rsltString (Penalty Result String)
 *          vNMS_Opportunity.Extra_Input13 = paramValue (Param Field Identifier)
 *-------------------------------------------------------------------------------------------------------------------------*/

    Public Static String earlyTermination(String pnltID,String status, String rsltString, String paramValue){
        
        System.debug('Early Termination Charges Asynchronous Call :: Penalty ID: '+pnltID+' Param Field: '+paramValue);
               
        List<NFT_Dummy_Object__c> nftList = new List<NFT_Dummy_Object__c>();
        List<SObject> nftList1 = new List<NFT_Dummy_Object__c>();
        Penalty_Calculation_Field_Configuration__c pnltConfig = new Penalty_Calculation_Field_Configuration__c();

        nftList = [Select Id, Penalty_Request_Id__c, Completion_Status__c from NFT_Dummy_Object__c where Penalty_Request_Id__c =:pnltID];
        
        if(nftList!=null && nftList.Size()!=0){
            NFT_Dummy_Object__c nftObj = new NFT_Dummy_Object__c();
            Sobject nftSObj = (Sobject)nftObj;
            nftSObj.put('Id',nftList[0].Id);
            pnltConfig.Field_Name__c = Penalty_Calculation_Field_Configuration__c.getValues(paramValue).Field_Name__c;
            nftSObj.put(pnltConfig.Field_Name__c,rsltString);
            
            if(status!=null && status !=''){
                nftSObj.put('Completion_Status__c',status);
            }
            nftList1.add(nftSObj);
            update nftList1;
            return Label.updateSuccess;
        }
        else
            return Label.NoOutput;
    }

/*-------------------------------------------------------------------------------------------------------------------------*
 * Method Name : emailDistribution
 * Description : This Method will receive the request from Admin Module for Enabling/Disabling a Contact in SFDC and update
 *          Disabled_in_Geneva__c field accordingly
 * Attributes  : vNMS_Opportunity.Extra_Input1  = billAccNo (Billing Account Number)
 *          vNMS_Opportunity.Extra_Input2   = emailID (Email Id)
 *          vNMS_Opportunity.Extra_Input11 = status (Contact Status - Enabled/Disabled)
 *-------------------------------------------------------------------------------------------------------------------------*/
    
    Public Static String emailDistribution(String billAccNo, String emailID, String status){
        
        System.debug('Email Distribution Operation :: Acc Num: '+billAccNo+' Email: '+emailID+' Status: '+status);
        
        List<Billing_Account__c> billObjList = new List<Billing_Account__c>();
        billObjList = [Select Id, Contact__c from Billing_Account__c where Billing_Account_Number__c = :billAccNo and Contact__r.Email = :emailID];
        if(billObjList.size()!=0 && billObjList !=null){
            Contact conObj = new Contact(Id=billObjList[0].Contact__c);
            if(status.equalsIgnoreCase('Enabled')){
                conObj.Disabled_in_Geneva__c=False;
            }
            else{
                conObj.Disabled_in_Geneva__c=True;
            }
            update conObj;
            return Label.updateSuccess;
        }
        else
            return Label.NoOutput;  
    }

/*-------------------------------------------------------------------------------------------------------------------------*
 * Method Name : mediaUpdate
 * Description : This Method will receive the request from BPM for updating the Existing Media/Provider of a Service which
 *          was changed at BPM during Task Closure
 * Attributes  : vNMS_Opportunity.Opportunity_Id = oppId (Opportunity Id)
 *          	 vNMS_Opportunity.Extra_Input3   = serviceId (Service ID)
 *          	 vNMS_Opportunity.Extra_Input4   = siteAMedia1 (Site A Primary Media)
 *          	 vNMS_Opportunity.Extra_Input5   = siteAProvider1 (Site A Primary Provider)
 *          	 vNMS_Opportunity.Extra_Input6   = siteBMedia1 (Site B Primary Media)
 *          	 vNMS_Opportunity.Extra_Input7   = siteBProvider1 (Site B Primary Provider)
 *          	 vNMS_Opportunity.Extra_Input8   = siteAMedia2 (Site A Secondary Media)
 *          	 vNMS_Opportunity.Extra_Input9   = siteAProvider2 (Site A Secondary Provider)
 *          	 vNMS_Opportunity.Extra_Input11  = siteBMedia2 (Site B Secondary Media)
 *          	 vNMS_Opportunity.Extra_Input12  = siteBProvider2 (Site B Secondary Provider)
 *-------------------------------------------------------------------------------------------------------------------------*/
    
    Public Static String mediaUpdate(String oppId, String serviceId, String siteAMedia1, String siteAProvider1, String siteBMedia1, String siteBProvider1, String siteAMedia2, String siteAProvider2, String siteBMedia2, String siteBProvider2){
        
        System.debug('Media Backupdate from BPM Operation :: Opportunity ID: '+oppId+' Service Id: '+serviceID);
        
        List<Neotel_Product__c> neoProdList = new List<Neotel_Product__c>();
        neoProdList = [Select Id,Service_Id__c,Opportunity__r.Opportunity_Id__c from Neotel_Product__c where Service_Id__c = :serviceId and Opportunity__r.Opportunity_Id__c = :oppId];
        
        if(neoProdList!=null && neoProdList.Size() != 0){
            Neotel_Product__c neoProdObj = new Neotel_Product__c(Id=neoProdList[0].Id);
            if(siteAMedia1!=null)
                neoProdObj.Media__c=siteAMedia1;
            if(siteAProvider1!=null)
                neoProdObj.Provider__c=siteAProvider1;
            if(siteBMedia1!=null)
                neoProdObj.Site_B_Primary_Media__c=SiteBMedia1;
            if(siteBProvider1!=null)
                neoProdObj.Provider_Site_B__c=siteBProvider1;
            if(siteAMedia2!=null)
                neoProdObj.Site_A_Secondary_Media__c=siteAMedia2;
            if(siteAProvider2!=null)
                neoProdObj.Site_A_Secondary_Provider__c=siteAProvider2;
            if(siteBMedia2!=null)
                neoProdObj.Site_B_Secondary_Media__c=SiteBMedia2;
            if(siteBProvider2!=null)
                neoProdObj.Site_B_Secondary_Provider__c=siteBProvider2;
            update neoProdObj;
            return Label.updateSuccess;
        }
        else
            return Label.NoOutput;  
    }
    
/*-------------------------------------------------------------------------------------------------------------------------*
 * Method Name : provisioningUpdate
 * Description : This Method will receive the request from BPM for updating the E2E Status of the Services in SFDC once the
 *          	 Tasks are closed and Provisioning is completed for the Services
 * Attributes  : vNMS_Opportunity.Opportunity_Id = oppId (Opportunity Id)
 *          	 vNMS_Opportunity.Extra_Input2   = serviceID (Service Id)
 *          	 vNMS_Opportunity.Extra_Input1   = e2eStatus (E2E Status)
 *          	 vNMS_Opportunity.Extra_Input3   = commDate (Commission Date)
 * 				 vNMS_Opportunity.Extra_Input4   = comments (Win/Loss Comments)//V1.5
 *-------------------------------------------------------------------------------------------------------------------------*/
    
    Public Static String provisioningUpdate(String oppId, String serviceID, String e2eStatus, String commDate,String comments){/*V1.5 Added comments paramter*/
        
        System.debug('Provisioning Update Operation :: Opportunity Id: '+oppId+' Service Id: '+serviceId+' E2E Status: '+e2eStatus);
        /*V1.5 Starts*/
        if (e2eStatus == 'Active')
        {
            string RepricingUpdate = UpdateRejectRepricing(oppId,serviceID,e2eStatus,comments);
            return RepricingUpdate;
        }/*V1.5 Ends*/
        else{ //1.5 Added Else
        List<Neotel_Product__c> neoProdList = new List<Neotel_Product__c>();
        List<Neotel_Product__c> repricingList = new List<Neotel_Product__c>();
        Integer listSize;
        neoProdList = [Select Id, Service_Id__c, E2E_Status__c, Opportunity__r.Opportunity_Id__c, Opportunity__r.Id, Opportunity__r.Type from Neotel_Product__c where Service_Id__c = :serviceID and Opportunity__r.Opportunity_Id__c = :oppId Limit 1];
        listSize = neoProdList.size();
        if(neoProdList != Null && listSize != 0){
            if(commDate != Null && commDate !=''){
                neoProdList[0].Commissioned_Date__c = Date.valueOf(commDate);
            }                
            neoProdList[0].E2E_Status__c = e2eStatus;
            if((e2eStatus == 'Cancelled') && (neoProdList[0].Opportunity__r.Type !='Cancellation')){
                repricingList = [Select Id, Service_Id__c, E2E_Status__c, Opportunity__r.Opportunity_Id__c from Neotel_Product__c where Service_Id__c = :neoProdList[0].Service_Id__c and Opportunity__r.Parent_Opportunity__c = :neoProdList[0].Opportunity__r.Id and Opportunity__r.Probability = 100 and Opportunity__r.Type = 'Repricing' and E2E_Status__c Not in ('Provisioned','Cancelled','Terminated')];
                listSize = repricingList.size();
                
                if(listSize != 0){
                    for(Integer i=0; i<listSize; i++){
                        System.debug('Repricing Order Cancellation Update :: '+repricingList[i].Service_Id__c+' :: '+repricingList[i].Opportunity__r.Opportunity_Id__c);
                        repricingList[i].E2E_Status__c = e2eStatus;  
                    }
                    update repricingList;
                }
            }
            System.debug('E2E Status update of the Order sent by BPM');
            update neoProdList;
            return Label.updateSuccess;
        }
        else
            return Label.NoOutput;
    }//V1.5 End of Else
    }
    
/*-------------------------------------------------------------------------------------------------------------------------*
 * Method Name : serviceRecordUpdate
 * Description : This Method will receive the request from BPM for updating the Commission Date/Termination Date in Service
 *               Records
 * Attributes  : vNMS_Opportunity.Opportunity_Id = oppId (Opportunity Id)
 *          	 vNMS_Opportunity.Extra_Input1   = serviceID (M6Service Id)
 *          	 vNMS_Opportunity.Extra_Input2   = status (Service Status)
 *          	 vNMS_Opportunity.Extra_Input3   = commDate (Commission/Termination Date)
 *-------------------------------------------------------------------------------------------------------------------------*/
    
    Public Static String serviceRecordUpdate(String oppID, String m6ServiceID, String status, String commDate){
        
        System.debug('Service Record Update Operation :: Service Id: '+m6ServiceID+' Opportunity Id: '+oppId+' Commission Date: '+commDate);
        
        List<Service_Record__c> srList = new List<Service_Record__c>();
        srList = [Select id from Service_Record__c where M6_Service_ID__c = :m6ServiceID and Opportunity_ID__c = :oppID and Service_Status__c = 'In Progress' and SR_Oppotunty_Sub_Type__c not in('Price Revision','On Demand Recording','Professional Services Upgrade','Routing Change','Unbundling','Creation of Corporate CUG','Renewal Of Contract') and Type_of_Service__c != 'NeoTranstel Voice'];
        if(srList !=null && srList.size() !=0){
            Service_Record__c srObj = new Service_Record__c(Id=srList[0].Id);
            
            if(commDate != Null && commDate !=''){
                if(!(String.isBlank(status)) && (status.equalsIgnoreCase('Termination'))){
                   srObj.Termination_Date__c=Date.valueOf(commDate); 
                }
                else{
                    srObj.Commission_Date__c=Date.valueOf(commDate);
                }
            }
            else if(!(String.isBlank(status)) && (status.equals('Cancelled')))
                srObj.Service_Status__c=status;
            
            update srObj;
            return Label.updateSuccess;
        }
        else
            return Label.NoOutput;    
    }

/*-------------------------------------------------------------------------------------------------------------------------*
 * Method Name : billIMACDAuto
 * Description : This Method will receive the response from Geneva containing IMACD Account Number for which the IMACD
 *          	 Billing request is initiated from SFDC(Asynchronous Call)         
 * Attributes  : vNMS_Opportunity.Extra_Input4   = IMACDId (IMACD_Name__c)
 *          	 vNMS_Opportunity.Extra_Input1   = accNum (IMACD_Account_Number__c)
 *          	 vNMS_Opportunity.Extra_Input2   = errorMsg (E2E_Reason__c)
 *-------------------------------------------------------------------------------------------------------------------------*/
    
    Public Static String billIMACDAuto(String IMACDId, String accNum, String errorMsg){
        
        System.debug('Billing Account Number update on IMACD :: IMACD Name: '+IMACDId+' IMACD Billing Account number: '+accNum+' E2E Reason: '+errorMsg);
        
        List<IMACD__c> imacdList= new List<IMACD__c>();
        imacdList=[select id,IMACD_Name__c from IMACD__c where IMACD_Name__c=:IMACDId limit 1];
        if(imacdList != null && imacdList.size() > 0){
            imacdList[0].IMACD_Account_Number__c=accNum;
            if(errorMsg=='SUCCESS'){
                imacdList[0].E2E_Reason__c='SUCCESS|';
            }
            else{
                imacdList[0].E2E_Reason__c=errorMsg;
            }
            update imacdList;
            return Label.updateSuccess;
        }
        else
            return Label.NoOutput;     
    }
/*V1.2 Ends*/   

/*v1.3 starts*/
    
/*-------------------------------------------------------------------------------------------------------------------------*
 * Method Name : kamUpdate
 * Description : This Method will receive the KAM Update Response from BPM and will update the same in the Opportunity or User record.
 * Attributes  : vNMS_Opportunity.Extra_Input1 = Error_Reason
 *               vNMS_Opportunity.Extra_Input2 = Operation name of Type of KAM Change
 *               vNMS_Opportunity.Extra_Input3 = Username or list opty
 *               vNMS_Opportunity.Extra_Input4 = Error Flag(T or F)
 *               vNMS_Opportunity.Extra_Input5 = Error Message(Success or Failure)
 *-------------------------------------------------------------------------------------------------------------------------*/    
    Public Static String kamUpdate(String reason, String opName, String reqList,String err_flag,String err_msg){
        String errorMsg;
        if(opName.equals('User_Deactivation'))
        {
            system.debug('reqList'+reqList);
            system.debug('Inside User Deactivation');
            List<String> multUser = new List<String>();
            multUser=reqList.split('#');
            List<User> userObjIns = new List<User>();
            userObjIns = [SELECT Name, FirstName, LastName,IsActive,Email,Username, MobilePhone, ID FROM USER WHERE Username =: multUser];
            
            if(userObjIns!=null && userObjIns.size()>0)
            {
                system.debug('Inside IF loop for UD');
                for(integer i=0;i<userObjIns.size();i++)
                {
                    if(err_flag.equals('T') || err_msg.equals('SUCCESS'))
                    {
                        system.debug('Response is success');
                        userObjIns[i].KAM_Flow_to_BPM_Status__c = 'Success';
                        errorMsg=Label.updateSuccess;
                    }
                    else if(err_flag.equals('F') || err_msg.equals('FAILURE'))
                    {
                        system.debug('Response is failure');
                        userObjIns[i].KAM_Flow_to_BPM_Status__c = 'KAM update failed with reason:'+reason;
                        errorMsg='Failure';
                    }
                }
                update userObjIns;
            }
            else
                errorMsg='User does not exists with specified Username';
        }
        else if(opName.equals('Opportunity_Owner_Change'))
        {
            List<String> mulOpty = new List<String>();
            mulOpty=reqList.split('#');
            system.debug('mulOpty'+mulOpty);
            List<Opportunity> optyObjIns = new List<Opportunity>();
            optyObjIns=[select id,Opportunity_Id__c from Opportunity where Opportunity_Id__c=:mulOpty];
            system.debug('optyObjIns'+optyObjIns);
            system.debug('size'+optyObjIns.size());
            if(optyObjIns!=null && optyObjIns.size()>0)
            {
                for(integer i=0;i<optyObjIns.size();i++)
                {
                    if(err_flag.equals('T') || err_msg.equals('SUCCESS')){
                        system.debug('Response is success');
                        optyObjIns[i].Opportunity_Global_Sales_VP__c='KAM Details Updated successfully in BPM';
                        errorMsg=Label.updateSuccess;
                    }
                    else if(err_flag.equals('F') || err_msg.equals('FAILURE'))
                    {
                        system.debug('Response is failure');
                        optyObjIns[i].Opportunity_Global_Sales_VP__c='KAM update failed with reason:'+reason;
                        errorMsg='Failure';
                    }
                }
                update optyObjIns;
            }
            else
                errorMsg='Opportunity does not exists with specified Opportunity_Id';
        }
        if(err_flag=='F' || err_msg=='FAILURE')
        {
            System.debug('KAM Update failed in BPM  :' + reason);
            List<String> Admin_emails = label.System_Admin_Emails.split(',');
            EmailTemplate template29 = [select Id from EmailTemplate where name ='Emailfor29days'];
            String s = 'Dear System Admin,';
            if(opName.equals('User_Deactivation'))
                s+= '<p> KAM Details were not updated in BPM when the below user was deactivated</p>';
            else
                s+= '<p> KAM Details were not updated in BPM for the below opportunities</p>';
            s+=reqList;
            s+= '<p> Please check the below error</p>';
            s+= reason;
            s+= '<p><b> Thanks & Regards </b></p>'+'<p><b> SFDC</b></p>';
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setTemplateID(template29.Id);
            email.setSaveAsActivity(false);
            String subline = 'KAM Update failed in BPM';
            subline += Datetime.now().format('yyyy-MMM-dd');
            email.setSubject(subline);
            email.setHtmlBody(s);
            email.setToAddresses(Admin_emails);
            email.emailPriority = 'Highest';
            Messaging.sendEmail(New Messaging.SingleEmailMessage[]{email});
        }
        return errorMsg;
    }
/*v1.3 ends */

/*v1.4 starts*/
/*-------------------------------------------------------------------------------------------------------------------------*
 * Method Name : confidenceUpdate
 * Description : This Method will receive the Confidence level Update Response from BPM and will update the same in the Site Address.
 * Attributes  : vNMS_Opportunity.Extra_Input1  = confidenceLevel
 *          vNMS_Opportunity.Extra_Input2   = siteId
 *-------------------------------------------------------------------------------------------------------------------------*/
Public Static String confidenceUpdate(String confidenceLevel, String siteId){
    List<Site_Address__c> SiteAddressObjIns = new List<Site_Address__c>() ;
    SiteAddressObjIns = [SELECT id, Name, E2E_Site_Address_ID__c FROM Site_Address__c WHERE E2E_Site_Address_ID__c =: siteId AND E2E_Site_Address_ID__c!=null ];
    System.debug('SiteAddressObjIns : '+SiteAddressObjIns);
    if (SiteAddressObjIns!=null && SiteAddressObjIns.size()>0)
       {
         for(integer i=0;i<SiteAddressObjIns.size();i++)
          {
            system.debug('Response is success');
            SiteAddressObjIns[i].Confidence_Level__c = confidenceLevel;
          }
         update SiteAddressObjIns;
         return Label.updateSuccess;
       } 
    else
        return Label.NoOutput;  
 }
/*v1.4 ends */
/*v1.5 starts */
/*-------------------------------------------------------------------------------------------------------------------------*
 * Method Name : UpdateRejectRepricing
 * Description : This Method will receive the Update for Unholded Repricing Order from BPM to update the E2E status of 
 *               requested order from Re-pricing to New and CloseDrop or Delete the Current Opty or Product
 * Attributes  : vNMS_Opportunity.Opportunity_Id = oppId (Opportunity Id)
 *               vNMS_Opportunity.Extra_Input2   = serviceID (Service Id)
 *               vNMS_Opportunity.Extra_Input1   = e2eStatus (E2E Status)
 *               vNMS_Opportunity.Extra_Input3   = comments (Win/Loss Comments)
                Need to Test this for Git Lab
                Adding Another one line 
 *-------------------------------------------------------------------------------------------------------------------------*/
    Public Static String UpdateRejectRepricing(String opID,String serviceID,String e2eStatus,String comments){
        try{
            
                System.debug('Rejection of Repricing Operation :: Opportunity Id: '+OpID+' Service Id: '+serviceID+' E2E Status: '+e2eStatus+'Remarks: '+comments);
                
                String[] labelResponse = label.Repricing_Failure_Backupdate.split('#');
                
                Integer sizenpList = 0;
                List <Neotel_Product__c> npList = new List <Neotel_Product__c>();
                npList = [Select id,Service_Id__c,E2E_Status__c,Opportunity__r.Type,Opportunity__r.StageName,Opportunity__r.Parent_Opportunity__r.Opportunity_ID__c,Opportunity__r.Opportunity_ID__c,Opportunity__r.Drop_Reasons__c,Opportunity__r.Win_Loss_Remarks__c,Opportunity__r.No_of_Neotel_Products__c from Neotel_Product__c where opportunity__r.StageName Not In( 'Closed Dropped','Closed Lost','Closed Duplicate') and  e2e_status__c Not In ('Cancelled','Terminated') and Service_Id__c =:serviceID  order by Opportunity__r.Order_Accepted_On__c desc nulls first];
                sizenpList = npList.size();
                
                
                
             	if (sizenpList != 0){       
                    if (npList[0].Opportunity__r.Opportunity_ID__c == opID && npList[0].Service_Id__c == serviceID){
                            system.debug('inside the if Condition::::');
                            system.debug('OptytoModify:' +npList[0].Opportunity__r.Opportunity_ID__c+'::'+'Id of Service to Modify'+npList[0].id);

                            Neotel_Product__c UpdateOldProd = new Neotel_Product__c (Id = npList[0].id);
                            UpdateOldProd.E2E_Status__c = 'New';
                            Update UpdateOldProd;
                            return Label.updateSuccess;
                        }
                    
                    
                    else if (npList[1].Opportunity__r.Opportunity_ID__c == opID && npList[0].Service_Id__c == serviceID){
                            system.debug('inside the elseif Condition::::');
                            system.debug('OptytoModify:' +npList[0].Opportunity__r.Opportunity_ID__c+'::'+'Id of Service to Modify'+npList[1].id);
                            Opportunity UpdateOpp = new Opportunity(Id = npList[0].Opportunity__r.Id);
                            Neotel_Product__c UpdateOldProd = new Neotel_Product__c (Id = npList[1].id);
                        
                            if(npList[0].Opportunity__r.StageName != 'Closed Won – COF Received' &&  npList[0].Opportunity__r.StageName != 'Closed Won – Order Processing' && npList[0].Opportunity__r.StageName!= 'Closed Won – Order Accepted' && npList[0].Opportunity__r.No_of_Neotel_Products__c == 1)
                            {
                                system.debug('Inside the First Condition to Close drop and Update the E2E as New');
                                UpdateOpp.StageName = 'Closed Dropped';
                                UpdateOpp.Drop_Reasons__c = 'Rejection of Repricing';
                                UpdateOpp.Win_Loss_Remarks__c = comments;
                                UpdateOldProd.E2E_Status__c = 'New';
                                Update UpdateOpp;
                                Update UpdateOldProd;
                                return Label.updateSuccess;
                            }
                            else if(npList[0].Opportunity__r.StageName != 'Closed Won – COF Received' && npList[0].Opportunity__r.StageName != 'Closed Won – Order Processing' && npList[0].Opportunity__r.StageName!= 'Closed Won – Order Accepted' && npList[0].Opportunity__r.No_of_Neotel_Products__c > 1)
                            {
                                system.debug('Inside the Second Condition to Delete the Product and Update the E2E as New');
                                delete npList[0];
                                UpdateOpp.Win_Loss_Remarks__c = comments;
                                UpdateOldProd.E2E_Status__c = 'New';
                                Update UpdateOpp;
                                Update UpdateOldProd;
                                return Label.updateSuccess;
                            }
                            else if (npList[0].Opportunity__r.StageName == 'Closed Won – COF Received' || npList[0].Opportunity__r.StageName == 'Closed Won – Order Processing')
                            {
                                system.debug('Inside the Third Condition to Return failure');
                                return labelResponse[0];
                            }
                            else if (npList[0].Opportunity__r.StageName == 'Closed Won – Order Accepted')
                            {
                                system.debug('Inside the Fourth Condition to Return failure');
                                return labelResponse[1];
                            }
                      }
                    
				}
                return 'Given Service is Not Present in the Parent or Current Opty';
           }
        catch(Exception e){
            System.debug('Exception Occurred :: Line No. :: '+e.getLineNumber()+' Message :: '+e.getMessage());
            return 'Unable to process the request for following reason - /n'+e;
        }
    }
/*V1.5 Ends*/ 
}